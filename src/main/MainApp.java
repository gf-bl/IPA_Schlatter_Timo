package main;

import controller.MainViewController;
import controller.TableViewController;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import logic.GeometryTests;

/**
 * Starts and controlls the views
 *
 * @version 1.0
 * @author schlatter
 */
public class MainApp extends Application {

    private Stage primaryStage;
    private Scene scene;
    private MainViewController mainViewController;
    private Pane mainView;
    private Pane tableView;

    /**
     * Sets the stage and starts the application Override-method from abstract
     * class "Application"
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Functional Testing JTS Topology Suite");

        //loads the "MainView" and shows it
        //uses SceneController to switch scenes
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("/fxml/MainView.fxml"));
            //Scene scene = new Scene(FXMLLoader.load(this.getClass().getResource("/fxml/MainView.fxml")));
            mainView = loader.load();
            scene = new Scene(mainView);
            this.primaryStage.setScene(scene);
            this.primaryStage.show();
            this.mainViewController = loader.getController();
            this.mainViewController.setMainApp(this);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * loads the tableView and shows the results
     *
     * @param results
     */
    public void loadTableView(GeometryTests results) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("/fxml/TableView.fxml"));
            this.tableView = loader.load();
            TableViewController tableViewController = loader.getController();
            tableViewController.getResults(results);
            tableViewController.setMainApp(this);
            this.showTableView();

        } catch (IOException ex) {
            System.out.println("test");
            ex.printStackTrace();
        }
    }

    /**
     * shows TableView
     */
    public void showTableView() {
        scene.setRoot(this.tableView);
    }

    /**
     * Shows mainView
     */
    public void showMainView() {
        this.scene.setRoot(this.mainView);
    }
}
