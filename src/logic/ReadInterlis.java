package logic;

import ch.bl.gf.IOX2Hashtable;
import ch.interlis.iom.IomObject;
import ch.interlis.iom_j.xtf.XtfReader;
import ch.interlis.iox.EndBasketEvent;
import ch.interlis.iox.EndTransferEvent;
import ch.interlis.iox.IoxEvent;
import ch.interlis.iox.IoxException;
import ch.interlis.iox.IoxReader;
import ch.interlis.iox.ObjectEvent;
import ch.interlis.iox.StartBasketEvent;
import ch.interlis.iox.StartTransferEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * reads the interlis file and sets the objetcs into maps
 *
 * @author schlatter
 */
public class ReadInterlis {

    private static IoxReader ioxReader = null;
    private static IOX2Hashtable ioxHashTable = new IOX2Hashtable();
    private static int counter = 0;
    private Map<String, IomObject> polyMap;
    private Map<String, IomObject> polyWArcMap;
    private Map<String, String> infoMap;
    private Map<Object, Object> themes = new HashMap<>();
    private String referenceLiegenschaft = "Liegenschaft_von";
    private String referenceBodenbedeckung = "Gebaeudenummer_von";

    /**
     * reads the iomobjects into hashtable
     *
     * @param path
     * @throws IoxException
     */
    public void readData(String path) throws IoxException {

        if (!(path.equals(""))) {

            File xtfFile = new File(path);

            try {
                ioxReader = new XtfReader(xtfFile);
                IoxEvent event;

                do {
                    //use of events to get the objects / Separation of Controlling and data
                    event = ioxReader.read();

                    //the order of the events has to be as follows:
                    // start of transfer file
                    if (event instanceof StartTransferEvent) {

                        //start of basket    
                    } else if (event instanceof StartBasketEvent) {

                        //start of object    
                    } else if (event instanceof ObjectEvent) {

                        //iomObject is a format neutral transfer element
                        IomObject iomObject = ((ObjectEvent) event).getIomObject();

                        ioxHashTable.put(iomObject.getobjectoid(), iomObject);

                    } else if (event instanceof EndBasketEvent) {

                    } else if (event instanceof EndTransferEvent) {

                    }

                } while (!(event instanceof EndTransferEvent));

            } catch (IoxException ex) {
                ex.printStackTrace();

            } finally {
                if (ioxReader != null) {
                    ioxReader.close();
                }
            }
        } else {

        }
    }

    /**
     * analyzes the iomobjects and separates them into hashmaps
     *
     */
    public void analyze() {
        //Vector is synchronized
        Vector<IomObject> elements = null;

        //overwrites the Map everytime it reads a interlis file
        this.polyMap = new HashMap<>();
        this.polyWArcMap = new HashMap<>();
        this.infoMap = new HashMap<>();

        for (Map.Entry<Object, Object> theme : this.themes.entrySet()) {
            //adds the elements from the HashTable to the vector list
            elements = ioxHashTable.getElementsOfClass(theme.getKey().toString(), elements);

            for (IomObject iomObj : elements) {
                //adds the geom objects to vector lists
                //Separated through ARC
                if (iomObj.getattrobj(theme.getValue().toString(), 0) != null) {

                    // get reference id and add them to reference map to combine poly and number for map
                    if (iomObj.getattrobj(theme.getValue().toString(), 0).toString().contains("ARC")) {
                        //Liegenschaften
                        if (iomObj.getattrobj(this.referenceLiegenschaft, 0) != null) {
                            this.polyWArcMap.put(iomObj.getattrobj(this.referenceLiegenschaft, 0).getobjectrefoid(), iomObj.getattrobj(theme.getValue().toString(), 0));
                        } else {
                            //Bodenbedeckung
                            this.polyWArcMap.put(iomObj.getobjectoid(), iomObj.getattrobj(theme.getValue().toString(), 0));
                        }

                    } else {

                        //sets the key the which references to liegenschaftsnummer
                        //Liegenschaften
                        if (iomObj.getattrobj(this.referenceLiegenschaft, 0) != null) {
                            this.polyMap.put(iomObj.getattrobj(this.referenceLiegenschaft, 0).getobjectrefoid(), iomObj.getattrobj(theme.getValue().toString(), 0));
                        } else {
                            //Bodenbedeckung
                            this.polyMap.put(iomObj.getobjectoid(), iomObj.getattrobj(theme.getValue().toString(), 0));
                        }
                        //sets the key the which references to liegenschaftsnummer

                    }

                } else if (iomObj.getattrvalue(theme.getValue().toString()) != null) {

                    //Bodenbedeckung
                    if (iomObj.getattrobj(this.referenceBodenbedeckung, 0) != null) {
                        this.infoMap.put(iomObj.getattrobj(this.referenceBodenbedeckung, 0).getobjectrefoid(), iomObj.getattrvalue(theme.getValue().toString()));
                        //Liegenschaften
                    } else {
                        this.infoMap.put(iomObj.getobjectoid(), iomObj.getattrvalue(theme.getValue().toString()));
                    }

                } else {
                    System.out.println("There was an error");
                }
            }
            System.out.println("FINISHED");
        }
    }

    /**
     * sets the themes from the property file
     *
     * @param themes
     */
    public void setTheme(Map.Entry<Object, Object> themes) {
        this.themes.put(themes.getKey(), themes.getValue());
    }

    /**
     * returns polyMap
     *
     * @return
     */
    public Map<String, IomObject> getPolyMap() {
        return polyMap;
    }

    /**
     * returns polyWArcMap
     *
     * @return
     */
    public Map<String, IomObject> getPolyWArcMap() {
        return polyWArcMap;
    }

    /**
     * returns information map with Gebäudenummers / Grundstücknummern
     *
     * @return
     */
    public Map<String, String> getInfoMap() {
        return infoMap;
    }
}
