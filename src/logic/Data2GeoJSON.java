package logic;

import ch.interlis.iom.IomObject;
import static ch.interlis.iox_j.jts.Iox2jts.surface2JTS;
import com.vividsolutions.jts.geom.Polygon;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

/**
 * class to create the geojson
 *
 * @author schlatter
 */
public class Data2GeoJSON {

    private static final String SLASH = File.separator;

    /**
     * creates the geojson on the given directory
     *
     * @param filePath
     * @param polygons
     * @param informations
     */
    public void createGeoJSON(String filePath, Map<String, IomObject> polygons, Map<String, String> informations) {
        try {
            File file = new File(filePath + SLASH + "geometry.geojson");
            SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
            builder.setName("TypeBuilder");
            builder.setCRS(CRS.decode("urn:ogc:def:crs:EPSG::2056"));

            //the required data for the feature
            builder.add("Geometrie", Polygon.class);
            builder.add("Nummer", String.class);
            //builder.add("Grundstuecknummer",String.class);
            final SimpleFeatureType TYPE = builder.buildFeatureType();

            FeatureJSON featureJson = new FeatureJSON();
            List<SimpleFeature> list = new ArrayList<>();

            // iterate through all polygon iomobjects
            for (Map.Entry<String, IomObject> entry : polygons.entrySet()) {
                //build the jts polygon and add it to the feature
                Polygon polygon = surface2JTS(entry.getValue(), 0);
                SimpleFeatureBuilder build = new SimpleFeatureBuilder(TYPE);
                build.add(polygon);
                //Get the informations like gebaeudenummer with same key and add it
                for (Map.Entry<String, String> informationEntry : informations.entrySet()) {
                    if (informationEntry.getKey().equals(entry.getKey())) {
                        build.add(informationEntry.getValue());
                    }
                }
                // build and add it to feature
                SimpleFeature simpleFeature = build.buildFeature(null);
                list.add(simpleFeature);

            }
            // add it to featurecollection
            SimpleFeatureCollection collection = new ListFeatureCollection(TYPE, list);
            featureJson.writeFeatureCollection(collection, file);

            System.out.println("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
