package tools;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

/**
 * the logger class
 *
 * @author schlatter
 */
public class Logger {

    private static FileHandler fileTxt;
    private static SimpleFormatter formatterTxt;
    private static final String SLASH = File.separator;

    /**
     * setup for the logger
     *
     * @param path
     */
    public static void setup(String path) {

        try {
            java.util.logging.Logger logger = java.util.logging.Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);

            //all messages above this level will be written to console
            logger.setLevel(Level.INFO);
            fileTxt = new FileHandler(path + SLASH + "log.txt");

            formatterTxt = new SimpleFormatter();
            fileTxt.setFormatter(formatterTxt);
            logger.addHandler(fileTxt);

        } catch (IOException | SecurityException ex) {
            ex.printStackTrace();
        }
    }
}
