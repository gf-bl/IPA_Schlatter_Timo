package controller;

import java.util.List;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import logic.GeometryTests;
import main.MainApp;
import model.Result;

/**
 * FXML controller class for the TableView
 *
 * @author schlatter
 */
public class TableViewController {

    @FXML
    private TableView<Result> tableView;

    @FXML
    private TableColumn<Result, String> columnOId;

    @FXML
    private TableColumn<Result, String> columnTestName;

    @FXML
    private TableColumn<Result, String> columnResultJts;

    @FXML
    private TableColumn<Result, String> columnResultBl;

    @FXML
    private TableColumn<Result, String> columnComparison;

    @FXML
    private Button btnMainMenu;

    private GeometryTests results;
    private List<Map<String, List<String>>> listLine;
    private List<Map<String, List<String>>> listPoly;
    private List<Map<String, List<String>>> listPolyWArc;
    private ObservableList<Result> resultList = FXCollections.observableArrayList();
    private MainApp mainApp;

    /**
     * leads back to the MainMenu
     */
    @FXML
    private void backToMainMenu() {
        this.mainApp.showMainView();
    }

    /**
     * Get the results
     *
     * @param results
     */
    public void getResults(GeometryTests results) {
        this.results = results;
        listLine = results.getListLine();
        listPoly = results.getListPoly();
        listPolyWArc = results.getListPolyWArc();
        this.addItems();
        this.fillTableView();
        this.tableView.setItems(resultList);
    }

    /**
     * Sets values for the columns
     */
    public void fillTableView() {
        this.columnOId.setCellValueFactory(cellData -> cellData.getValue().getObjectId());
        this.columnTestName.setCellValueFactory(cellData -> cellData.getValue().getTestName());
        this.columnResultJts.setCellValueFactory(cellData -> cellData.getValue().getResultJts());
        this.columnResultBl.setCellValueFactory(cellData -> cellData.getValue().getResultBl());
        this.columnComparison.setCellValueFactory(cellData -> cellData.getValue().getComparison());
    }

    /**
     * add all items to the table
     */
    public void addItems() {

        for (Map<String, List<String>> map : this.listLine) {
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                String key = entry.getKey();
                List<String> values = entry.getValue();
                this.resultList.add(new Result(key, values.get(0), values.get(1), values.get(2), values.get(3)));
            }
        }

        for (Map<String, List<String>> map : this.listPoly) {
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                String key = entry.getKey();
                List<String> values = entry.getValue();
                this.resultList.add(new Result(key, values.get(0), values.get(1), values.get(2), values.get(3)));
            }
        }

        for (Map<String, List<String>> map : this.listPolyWArc) {
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                String key = entry.getKey();
                List<String> values = entry.getValue();
                this.resultList.add(new Result(key, values.get(0), values.get(1), values.get(2), values.get(3)));
            }
        }
    }

    /**
     * sets the mainapp to switch scenes
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
}
