package model;

import javafx.beans.property.SimpleStringProperty;

/**
 * manages the result values
 *
 * @author schlatter
 */
public class Result {

    private SimpleStringProperty objectId;
    private SimpleStringProperty testName;
    private SimpleStringProperty resultJts;
    private SimpleStringProperty resultBl;
    private SimpleStringProperty comparison;

    /**
     * sets the values of the variables
     *
     * @param objectId
     * @param testName
     * @param resultJts
     * @param resultBl
     * @param comparison
     */
    public Result(String objectId, String testName, String resultJts, String resultBl, String comparison) {
        this.objectId = new SimpleStringProperty(objectId);
        this.testName = new SimpleStringProperty(testName);
        this.resultJts = new SimpleStringProperty(resultJts);
        this.resultBl = new SimpleStringProperty(resultBl);
        this.comparison = new SimpleStringProperty(comparison);
    }

    /**
     *
     * @return objectId
     */
    public SimpleStringProperty getObjectId() {
        return objectId;
    }

    /**
     *
     * @return testName
     */
    public SimpleStringProperty getTestName() {
        return testName;
    }

    /**
     *
     * @return resultJts
     */
    public SimpleStringProperty getResultJts() {
        return resultJts;
    }

    /**
     *
     * @return resultBl
     */
    public SimpleStringProperty getResultBl() {
        return resultBl;
    }

    /**
     *
     * @return comparison
     */
    public SimpleStringProperty getComparison() {
        return comparison;
    }
}
