var map;
var crs = new L.Proj.CRS('EPSG:2056',
  "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs",
  {
    resolutions: [65536, 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5, 0.25, 0.125] // 3 example zoom level resolutions
  }
),
map = L.map('map', {
        crs: crs,
        continuousWorld: true,
        worldCopyJump: false,
        minZoom: 10,
        maxZoom: 18
        
}).setView([47.48837212934008, 7.726988196372987], 10);


//tilelayer for the map
var tileLayer = L.tileLayer.wms('http://geowms.bl.ch', { //TileLayer vom Geoview Baselland
    layers: 'grundkarte_farbig_group', //Grundkarte farbig ausgewählt
    attribution: 'Map data &copy; <a href="https://www.baselland.ch/politik-und-behorden/direktionen/volkswirtschafts-und-gesundheitsdirektion/amt-fur-geoinformation">GIS Basel-Landschaft</a>',
    noWrap: true
}).addTo(map);

// get geojson with ajax call 
var geojson = new L.GeoJSON.AJAX('geometry.geojson', {
	middleware:function(data) {
		L.Proj.geoJson(data, {
			onEachFeature: function(feature, layer) {
                layer.bindPopup("Gebäude/Grundstücknummer: " + feature.properties.Nummer)
			}
		}).addTo(map);
	}
});

// shows on click lat and lng coordinates
map.on('click', function(e) {
    map.wrapLatLng(e.latlng);
    console.log(crs.project(e.latlng));
    alert(crs.project(e.latlng));
});

//sets View to the input coordinates
document.getElementById("btn").addEventListener("click", function() {
    var projectionCH = "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs";
    var x = parseInt(document.getElementById("coordinatesX").value);
    var y = parseInt(document.getElementById("coordinatesY").value);
    console.log("x:" + x + "y: " + y);
    
    var coord = proj4(projectionCH, 'EPSG:4326').forward([x,y]);
    map.setView([coord[1],coord[0]],10);
});


 
